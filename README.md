# Sosafe test

Sosafe test es una aplicación que resuelve la prueba técnica de Sosafe, está escrita en el framework Angular 11 del lado del cliente y NodeJS con NestJS 7.6.15 del lado del servidor. La idea de la aplicación es listar los eventos que se van creando en un mapa, permitiendo a todos los clientes conectados ver los eventos que se van creando en tiempo real.

## Comenzando
Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas.

## Pre-requisitos
- Node v14.0.0 
- Npm 6.14.8
- Angular cli 11.0.2
- NestJS 7.6.0

## Instalación

### Sosafe test server and Sosafe test app
Debes ir a las carpetas correspondientes (mencionadas en el título del apartado) y en cada una ejecutar:

```bash
npm install
```

## Ejecución

### Sosafe test server

```bash
npm run start
```

### Sosafe test app

```bash
ng serve
```

## Descripción funcional de la aplicación

- **Centrar mapa**: Si se permite acceder a la localización, la aplicación intentará centrar el mapa en su ubicación actual, sino, lo centrará en el último evento creado en la BD (Todas las coordenadas estan marcadas en Venezuela). En caso de querer centrar el mapa en una coordenada en específico, puede ir a la opción y desplegar el modal para indicar latitud y longitud.

- **Crear evento**: Permitirá crear un evento y todos los clientes conectados en la aplicación podrán visualizar el mismo si está contemplado dentro de sus keywords. Nota importante: En caso de que se creen múltiples eventos para una misma coordenada, entonces se agregará a la descripción del marcador, a la que puedes acceder dándole click al mismo.

- **Filtrar eventos**: Se pueden filtrar los eventos creados en la aplicación a través de keywords, separados por coma, los mismos son persistentes y se mantendrán los mismos cada vez que se acceda a la aplicación, en caso de que se cree un nuevo evento, solo se listarán si correponde con los keywords que se hayan escrito.

- **Contador de eventos**: Se muestra un contador de los eventos filtrados para cada cliente.

## License
[MIT](https://choosealicense.com/licenses/mit/)