import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { EventDocument, Event } from '../../schemas/event.schema';
import { EventDto } from '../../entities/event.dto';
import { EventGateway } from '../../event.gateway';

@Injectable()
export class EventsService {
    constructor(@InjectModel(Event.name) private eventModel: Model<EventDocument>, private gatewayService: EventGateway) {
    }

    public async getAllEvents(keyword: string): Promise<EventDocument[]> {
        if (keyword.trim().length == 0) {
            return await this.eventModel.find().exec();
        } else {
            const keywords = keyword.split(',')
            let regex = this.buildRegex(keywords);
            return await this.eventModel.find({ description: { $regex: regex, $options: 'i' } }).exec();
        }
    }

    public buildRegex(keywords: string[]) {
        let regex = ''
        keywords.forEach((keyword, i) => {
            if (i == keywords.length - 1) {
                regex += `.*${keyword.toLowerCase().trim()}.*`
            } else {
                regex += `.*${keyword.toLowerCase().trim()}.*|`
            }
        });
        return regex;
    }

    public async saveEvent(event: EventDto): Promise<any> {
        const eventFound = await this.findOneEvent(event.lat, event.lon);

        if (eventFound) {
            eventFound.description = eventFound.description + '<br>' + event.description;
            this.gatewayService.server.emit('saveEvent', eventFound);
            return await eventFound.save();
        } else {
            let createdEvent = new this.eventModel(event);
            this.gatewayService.server.emit('saveEvent', createdEvent);
            return await createdEvent.save();
        }
    }

    public async findOneEvent(lat: number, lon: number): Promise<EventDocument> {
        const eventFound = await this.eventModel.findOne({ lat: lat, lon: lon }).exec();
        return eventFound;
    }

}
