import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { EventsService } from '../../services/events/events.service';
import { EventDto } from '../../entities/event.dto';
import { EventGateway } from '../../event.gateway';

@Controller('events')
export class EventsController {
    constructor(private eventService: EventsService, private gatewayService: EventGateway) {

    }

    @Get()
    async getEvents(@Query('keyword') keyword: string = '') {
        const events = await this.eventService.getAllEvents(keyword)
        return {
            events
        }
    }

    @Post()
    create(@Body() event: EventDto) {
        const eventNew = this.eventService.saveEvent(event);
        return eventNew;
    }

}
