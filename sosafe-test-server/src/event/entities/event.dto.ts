export class EventDto {
    description: string;
    lat: number;
    lon: number;
}

