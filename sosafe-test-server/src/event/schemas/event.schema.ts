import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type EventDocument = Event & Document;

@Schema()
export class Event {
    @Prop()
    description: string;
    @Prop()
    lat: number;
    @Prop()
    lon: number;
}

export const EventSchema = SchemaFactory.createForClass(Event);