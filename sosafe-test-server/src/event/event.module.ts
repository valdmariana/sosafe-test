import { Module } from '@nestjs/common';
import { EventGateway } from './event.gateway';
import { EventsController } from './controllers/events/events.controller';
import { EventsService } from './services/events/events.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Event, EventSchema } from './schemas/event.schema';

@Module({
    providers: [EventGateway, EventsService],
    imports: [
        MongooseModule.forFeature([{ name: Event.name, schema: EventSchema }]),
    ],
    controllers: [EventsController],
})
export class EventModule { }
