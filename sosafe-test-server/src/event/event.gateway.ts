import { WebSocketGateway, WebSocketServer, OnGatewayConnection, OnGatewayDisconnect } from '@nestjs/websockets';

@WebSocketGateway({ namespace: 'events' })
export class EventGateway implements OnGatewayConnection, OnGatewayDisconnect {

    @WebSocketServer() server;
    users: number = 0;

    async handleConnection(client: any) {
        this.users++;
        this.server.emit('users', this.users);
    }

    async handleDisconnect(client: any) {
        this.users--;
        this.server.emit('users', this.users);
    }
}