import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EventModule } from './event/event.module';

@Module({
  imports: [EventModule,
    MongooseModule.forRoot('mongodb+srv://marivaldr:ePM4pEegOIJCHHZA@reigncluster.ewv3i.mongodb.net/sosafe-events?retryWrites=true&w=majority')
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
