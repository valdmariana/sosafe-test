import { ComponentFactoryResolver, Inject, Injectable, Injector } from '@angular/core';
import { LatLng, Marker } from 'leaflet';
import { HtmlMarkerComponent } from '../components/html-marker/html-marker.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CreateEventData } from '../components/map-markers/map-markers.component';

export class MarkerData {
  id!: string;
  description!: String;
  marker!: Marker;
  constructor(id: string, desc: string, marker: Marker) {
    this.id = id;
    this.description = desc;
    this.marker = marker
  }
}

export class EventData {
  _id!: string;
  description!: string;
  lon!: number;
  lat!: number;
}

@Injectable({
  providedIn: 'root'
})
export class MarkersService {
  private url = 'http://localhost:3000/events';
  private http: HttpClient;
  private options: any = {
    headers: new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
    })
  }

  markers: MarkerData[] = [];

  constructor(
    private resolver: ComponentFactoryResolver,
    private injector: Injector,
    @Inject(HttpClient) http: HttpClient) {
    this.http = http;
  }

  getEvents(keyword: string, map: any) {
    this.options.body = {};
    this.http.request<MarkerData>('get', this.url + '?keyword=' + keyword, this.options).subscribe((res: any) => {
      this.setMarkers(res.events, map);
    })
  }

  getMarkers() {
    return this.markers;
  }

  setMarkers(events: EventData[], map: any) {
    //Remover los markers que se filtraron
    let indexToDelete: any[] = [];
    this.markers.forEach((marker, i) => {
      const oldOne = events.find(event => event._id == marker.id);
      if (!oldOne) {
        indexToDelete.push(i)
        this.removeMarker(marker, map);
      }
    })
    indexToDelete.sort(function (a, b) { return b - a; });
    indexToDelete.forEach(index => this.markers.splice(index, 1))

    //Agrega los markers nuevos
    events.forEach((event, i) => {
      const newOne = this.markers.find(marker => marker.id == event._id);
      if (!newOne) {
        const markerNew = new MarkerData(event._id, event.description, new Marker(new LatLng(event.lat, event.lon)));
        this.markers.push(markerNew)
        this.addMarker(markerNew, map);
      }
    })
    if (this.markers.length > 0) {
      const latLngCenter = this.markers[this.markers.length - 1].marker.getLatLng()
      map.panTo(latLngCenter)
    }
  }

  setNewEventCreated(event: EventData, map: any, keyword: string) {
    const indexToUpdate = this.markers.findIndex(marker => marker.id == event._id);
    if (indexToUpdate != -1) {
      this.validateKeyword(keyword, event, map)
      this.bindPopup(this.markers[0])
    } else {
      this.validateKeyword(keyword, event, map);
    }
  }

  validateKeyword(keyword: string, event: EventData, map: any) {
    const keywords = keyword.split(',')
    const regex = this.buildRegex(keywords)
    if (event.description.toLowerCase().match(regex) != null) {
      const markerNew = new MarkerData(event._id, event.description, new Marker(new LatLng(event.lat, event.lon)));
      this.markers.push(markerNew)
      this.addMarker(markerNew, map);
    }
  }

  buildRegex(keywords: string[]) {
    let regex = ''
    keywords.forEach((keyword, i) => {
      if (i == keywords.length - 1) {
        regex += `.*${keyword.toLowerCase().trim()}.*`
      } else {
        regex += `.*${keyword.toLowerCase().trim()}.*|`
      }
    });
    return regex;
  }

  addMarker(markerData: MarkerData, map: any) {
    this.bindPopup(markerData)
    map.addLayer(markerData.marker)
  }

  removeMarker(marker: MarkerData, map: any) {
    map.removeLayer(marker.marker)
  }

  createEvent(event: CreateEventData, map: any) {
    this.options.body = {};
    const content_ = JSON.stringify(event);
    this.options.body = content_;
    this.http.request('post', this.url, this.options).subscribe((res: any) => {
      map.panTo(new LatLng(res.lat, res.lon))
    })
  }

  bindPopup(markerData: MarkerData) {
    const component = this.resolver.resolveComponentFactory(HtmlMarkerComponent).create(this.injector);
    component.instance.data = { description: markerData.description };
    component.changeDetectorRef.detectChanges();
    let div = document.createElement('div');
    div.appendChild(component.location.nativeElement);
    markerData.marker.bindPopup(div)
  }
}
