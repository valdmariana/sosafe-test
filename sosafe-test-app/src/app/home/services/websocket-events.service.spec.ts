import { TestBed } from '@angular/core/testing';

import { WebsocketEventsService } from './websocket-events.service';

describe('WebsocketEventsService', () => {
  let service: WebsocketEventsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WebsocketEventsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
