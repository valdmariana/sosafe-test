import { Component, OnInit } from '@angular/core';
import { LatLng, MapOptions, TileLayer } from 'leaflet';
import { WebsocketEventsService } from '../../services/websocket-events.service';
import { MarkersService } from '../../services/markers.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

export class CreateEventData {
  description!: string;
  lon!: number;
  lat!: number;
  constructor() {
    this.description = '';
    this.lon = 0;
    this.lat = 0;
  }
}

export class LatLongCenter {
  lon!: number;
  lat!: number;
  constructor() {
    this.lon = 0;
    this.lat = 0;
  }
}

@Component({
  selector: 'app-map-markers',
  templateUrl: './map-markers.component.html',
  styleUrls: ['./map-markers.component.css']
})
export class MapMarkersComponent implements OnInit {

  map: any;
  event: CreateEventData = new CreateEventData();
  latLongCenter: LatLongCenter = new LatLongCenter()
  keyword: string;
  options: MapOptions = {
    layers: [
      new TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })
    ],
    zoom: 10,
    center: new LatLng(10.480594, -66.903603)
  };

  constructor(private _eventService: WebsocketEventsService,
    private _markerService: MarkersService,
    private modalService: NgbModal) {
    let keywordLocal = localStorage.getItem('keyword-sosafe-test');
    this.keyword = keywordLocal ? keywordLocal : '';
  }

  ngOnInit(): void {
    this._eventService.listen("saveEvent").subscribe((data: any) => {
      this._markerService.setNewEventCreated(data, this.map, this.keyword)
    })
  }

  launchModalCreate(content: any) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' });
  }

  launchModalCenter(content: any) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' })
  }

  onMapReady(map: any) {
    this.map = map;
    this._markerService.getEvents(this.keyword, this.map);
    navigator.geolocation.getCurrentPosition((res) => {
      this.map.panTo(new LatLng(res.coords.latitude, res.coords.longitude))
    });
  }

  centerMap(modal?: any) {
    this.map.panTo(new LatLng(this.latLongCenter.lat, this.latLongCenter.lon))
    if (modal) {
      modal.dismiss()
    }
  }

  filterEvents() {
    localStorage.setItem('keyword-sosafe-test', this.keyword)
    this._markerService.getEvents(this.keyword, this.map);
  }

  createEvent(modal: any) {
    this._markerService.createEvent(this.event, this.map);
    if (modal) {
      modal.dismiss()
    }
  }

  ngOnDestroy() {
    this._eventService.disconnect()
  }

}
