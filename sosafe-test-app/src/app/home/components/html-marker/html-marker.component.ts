import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-html-marker',
  templateUrl: './html-marker.component.html',
  styleUrls: ['./html-marker.component.css']
})
export class HtmlMarkerComponent implements OnInit {
  @Input() data: any;

  constructor() { }

  ngOnInit(): void {
  }

}
